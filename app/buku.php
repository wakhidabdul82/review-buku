<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku extends Model
{
    
    protected $table = 'buku';
    protected $fillable = ['judul', 'sinopsis', 'link', 'thumbnail', 'kategori_id'];

    public function kategori(){
        return $this->belongsTo('App\kategori');
        }

}
