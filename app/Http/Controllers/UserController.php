<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return view('user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = DB::table('user')->get();
        return view('user.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required', 
            'password' => 'required',            
            'profile_id' => 'required',
        ]);

        $namathumbnail = time().'.'.$request->thumbnail->extension();  
   
        $request->thumbnail->move(public_path('images'), $namathumbnail);

        $user = new user;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->profile_id = $request->profile_id;

        $user->save();
        
        return redirect('/user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('user.create', compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('user')->where('id', $id)->first();
        
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'name' => 'required',
            'email' => 'required', 
            'password' => 'required',
            'profile_id' => 'required',
         ]);
         
         $affected = DB::table('user')
              ->where('id', $id)
              ->update(
                  [
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password'  => $request['password'],
                    'profile_id'  => $request['profile_id']                       
                    ]
                    
                    );

        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('user')->where('id', $id)->delete();

        return redirect('/user');
    }
}
