<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::all();

        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = DB::table('profile')->get();
        return view('profile.create', compact('profile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'umur' => 'required',
            'Alamat' => 'required', 
            'bio' => 'required',
            
        ]);

        $namathumbnail = time().'.'.$request->thumbnail->extension();  
   
        $request->thumbnail->move(public_path('images'), $namathumbnail);

        $profile = new profile;

        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->bio = $request->bio;
        $profile->save();
        
        return redirect('/profile');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::findOrFail($id);
        return view('profile.create', compact('profile'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = DB::table('profile')->where('id', $id)->first();
        
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'umur' => 'required',
            'Alamat' => 'required', 
            'bio' => 'required',
         ]);
         
         $affected = DB::table('profile')
              ->where('id', $id)
              ->update(
                  [
                      'umur' => $request['umur'],
                      'alamat'  => $request['alamat'],
                      'bio'  => $request['bio']                        
                    ]
                    
                    );

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('profile')->where('id', $id)->delete();

        return redirect('/profile');
    }
}
