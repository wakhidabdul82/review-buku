<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buku;
use File;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = buku::all();

        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('buku.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'sinopsis' => 'required', 
            'link' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif',
            'kategori_id' => 'required',
        ]);

        $namathumbnail = time().'.'.$request->thumbnail->extension();  
   
        $request->thumbnail->move(public_path('images'), $namathumbnail);

        $buku = new buku;

        $buku->judul = $request->judul;
        $buku->sinopsis = $request->sinopsis;
        $buku->link = $request->link;
        $buku->thumbnail = $namathumbnail;
        $buku->kategori_id = $request->kategori_id;

        $buku->save();
        
        return redirect('/buku');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        return view('buku.show', compact('buku'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $buku = buku::findOrFail($id);
        return view('buku.edit', compact('buku', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'judul' => 'required',
            'sinopsis' => 'required', 
            'link' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif',
            'kategori_id' => 'required',
         ]);
        
         if($request->has('thumbnail')){
             $berita = Berita::find(id);

             $patch = 'images/';
             File::delete($patch . $buku->thumbnail);

             $namathumbnail = time().'.'.$request->thumbnail->extension();

             $buku-> judul= $request-> judul;
             $buku->sinopsis = $request->sinopsis ;
             $buku->link = $request->link ;
             $buku->thumbnail = $request->thumbnail;
             $buku->kategori_id = $request->kategori_id ;


             $buku-> save();
            }
         
         if ($request->has('thumbnail')){
            $buku = buku::find($id);

            $path = "images/";
            File::delete($path . $buku->thumbnail);

            $namathumbnail = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('images'), $namathumbnail);
        
            $buku->judul = $request->judul;
            $buku->sinopsis = $request->sinopsis;
            $buku->link = $request->link;
            $buku->thumbnail = $namathumbnail;
            $buku->kategori_id = $request->kategori_id;
            $buku->save();
            return redirect('/buku');
         }else {
            $buku = buku::find($id);
            $buku->judul = $request->judul;
            $buku->sinopsis = $request->sinopsis;
            $buku->link = $request->link;
            $buku->kategori_id = $request->kategori_id;
            $buku->save();
            return redirect('/buku');
         }


             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = buku::find($id);

        $path = "images/";
        File::delete($path . $buku->thumbnail);

        $buku->delete();

        return redirect('/delete');
    }
}
