<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Komentar;

class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $komentar = Komentar::all();

        return view('komentar.index', compact('komentar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $komentar = DB::table('komentar')->get();
        return view('komentar.create', compact('komentar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',                        
            'buku_id' => 'required',
            'isi' => 'required',
        ]);

        $namathumbnail = time().'.'.$request->thumbnail->extension();  
   
        $request->thumbnail->move(public_path('images'), $namathumbnail);

        $komentar = new komentar;

        $komentar->user_id = $request->user_id;
        $komentar->buku_id = $request->buku_id;
        $komentar->isi = $request->isi;
        
        $komentar->save();
        
        return redirect('/komentar');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $komentar = Komentar::findOrFail($id);
        return view('komentar.create', compact('komentar'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $komentar = DB::table('komentar')->where('id', $id)->first();
        
        return view('komentar.edit', compact('komentar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'user_id' => 'required',
            'buku_id' => 'required',             
            'isi' => 'required',
         ]);
         
         $affected = DB::table('komentar')
              ->where('id', $id)
              ->update(
                  [
                    'user_id' => $request['user_id'],
                    'buku_id' => $request['buku_id'],
                    'isi'  => $request['isi']                       
                    ]
                    
                    );

        return redirect('/komentar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('komentar')->where('id', $id)->delete();

        return redirect('/komentar');
    }
}
