<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    
    protected $table = 'kategori';
    protected $fillable = ['nama', 'deskripsi'];

    public function buku()
    {
        return $this->hasMany('App\buku');
    }

}
