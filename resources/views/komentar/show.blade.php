@extends('master')

@section('judul')
Detail Buku {{$buku->id}}

@endsection


@section('content')
<img src="{{asset('image'. $buku->poster)}}" alt="">
<h4>{{$buku->judul}}</h4>
<p>{{$buku->content}}</p>

@endsection