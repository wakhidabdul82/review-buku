@extends('master')

@section('judul')
<h1>Tambah Komentar</h1>

@endsection

@section('content')

<form action="/buku" method="post" enctype="multipart/form-data">
    @csrf
    
    <div class="form-group">
        <label>Kategori</label>
        <select name="buku_id" id="" class="form-control">
            <option value="">--Pilih genre--</option>
            @foreach ($buku as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>                
            @endforeach

        </select>
        @error('buku')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Isi Komentar</label>
        <textarea name="komentar" class="form-control" id="" cols="30" rows="10"></textarea>
        
        @error('komentar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection
        