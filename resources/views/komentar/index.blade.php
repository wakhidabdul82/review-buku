@extends('master')

@section('judul')
<h1>List Buku</h1>

@endsection

@section('content')
<a href="/buku/create" class="my-2 btn btn-primary">Tambah Buku</a>

<div class="row">
    @forelse ($buku as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('images/'. $item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{$item->judul}}</h5>
              <p class="card-text">{{Str::limit($item->content, 20)}}.</p>
              <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            </div>


    </div>
        
    @empty
    <h1>Data Buku Masih Kosong</h1>
        
    @endforelse

</div>


  </div>

@endsection
