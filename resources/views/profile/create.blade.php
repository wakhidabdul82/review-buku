@extends('master')

@section('judul')
<h1>Tambah profile</h1>

@endsection

@section('content')

<form action="/profile" method="post">
    @csrf
    <div class="form-group">
        <label for="body">umur</label>
        <input type="text" class="form-control" name="umur"  placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Alamat</label>
        <textarea name="alamat" class="form-control" id="" cols="30" rows="10"></textarea>
        
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    
    <div class="form-group">
        <label for="body">Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
        
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection
        