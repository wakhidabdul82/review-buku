@extends('master')

@section('judul')
<h1>List Profile</h1>

@endsection

@section('content')
<a href="/profile/create" class="mb-3 btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>                
                <th scope="col">Umur</th>
                <th scope="col">Alamat</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($profile as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->alamat>}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <a href="/profile/{{$value->id}}" class="btn btn-info">Detail</a>
                            <a href="/profile/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/profile/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="my-1 btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection
