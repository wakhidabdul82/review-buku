@extends('master')

@section('judul')
Edit profile {{$profile->nama}}

@endsection

@section('content')

<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="body">umur</label>
        <input type="text" class="form-control" value="{{$profile->umur}}" name="umur"  placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Alamat</label>
        <textarea name="alamat" class="form-control" id="" cols="30" rows="10">{{$profile->bio}}</textarea>
        
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="body">Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$profile->bio}}</textarea>
        
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection
        