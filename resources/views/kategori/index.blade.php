@extends('master')

@section('judul')

    <h1>Kategori</h1>

@endsection
@section('subjudul')

    List Kategori

@endsection
@section('content')
<a href="/kategori/create" class="mb-3 btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>                
                <th scope="col">Deskripsi</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>                       
                        <td>{{$value->deskripsi}}</td>
                        <td>
                            
                            <form action="/kategori/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/kategori/{{$value->id}}" class="btn btn-info">Detail</a>
                                <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" class="my-1 btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection
