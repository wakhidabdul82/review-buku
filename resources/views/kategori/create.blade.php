@extends('master')

@section('judul')
<h1>Tambah Kategori</h1>

@endsection

@section('content')

<form action="/kategori" method="post">
    @csrf
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" name="nama"  placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    </div>
    <div class="form-group">
        <label for="body">Deskripsi</label>
        <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10"></textarea>
        
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection
        