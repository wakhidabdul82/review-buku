@extends('master')

@section('judul')
Edit Kategori {{$kategori->nama}}

@endsection

@section('content')

<form action="/kategori/{{$kategori->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" value="{{$kategori->nama}}" name="nama"  placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
   
    </div>
    <div class="form-group">
        <label for="body">Deskripsi</label>
        <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>
        
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection
        