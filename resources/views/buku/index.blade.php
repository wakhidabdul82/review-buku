@extends('master')

@section('judul')
<h1>List Buku</h1>

@endsection

@section('content')
<a href="/buku/create" class="my-2 btn btn-primary">Tambah Buku</a>

<div class="row">
    @forelse ($buku as $item)
<<<<<<< HEAD
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('images/'.$item->thumbnail)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <span class="badge badge-info">{{$item->kategori->nama}}</span>
                  <h3>{{$item->judul}}</h3>
                  <p class="card-text">{{Str::limit($item->sinopsis, 50)}}</p>
                  <form action="/buku/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/buku/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>
                </div>
              </div>
        </div>
=======
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('images/'. $item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3>{{$item->judul}}</h3>
              <p class="card-text">{{Str::limit($item->content, 20)}}.</p>
              
              <form action="/buku/{{$item->id}}" method="post">
                @csrf
                @method('Delete')
                <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/buku/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                <input type="submit" class= "btn btn-danger btn-sm" value="Delete">
              
              </form>
            </div>


    </div>
        
>>>>>>> 511f5a5dcc0e53173d9e776fe6955645ad546ed1
    @empty
        <h3>Film Belum Ada</h3>
    @endforelse

</div>


</div>

@endsection
