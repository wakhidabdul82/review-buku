@extends('master')

@section('judul')
<h1>Tambah Buku</h1>

@endsection

@section('content')

<form action="/buku" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Judul Buku</label>
        <input type="text" class="form-control" name="judul"  placeholder="Masukkan judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Sinopsis</label>
        <textarea name="sinopsis" class="form-control" id="" cols="30" rows="10"></textarea>
        
        @error('sinopsis')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Link</label>
        <input type="text" class="form-control" name="link"  placeholder="Masukkan link">
        @error('link')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Thumbnail</label>
        <input type="file" class="form-control" name="thumbnail" >
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" id="" class="form-control">
            <option value="">--Pilih Kategori--</option>
            @foreach ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>                
            @endforeach

        </select>
        @error('kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection
        