<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

 


Route::resource('buku', 'BukuController');
Route::resource('kategori', 'KategoriController');
Route::resource('komentar', 'KomentarController');
Route::resource('profile', 'ProfileController');
Route::resource('user', 'UserController');

